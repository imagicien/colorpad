import themidibus.*; //Import the library

MidiBus myBus; // The MidiBus
PGraphics g;
int tintInterval;
int tintCounter;

ArrayList velocities = new ArrayList<Integer>();
int octave = 0;
int nbOctaves = 4;
int firstKey = octave * 12 + 36; //36 48;
int lastKey = firstKey + nbOctaves * 12; //84 96;
int numKeys = lastKey - firstKey + 1;
float baseKeyWidth;

int blueEnd = 30;
int redStart = 100;
float pente;
float ordOrig;

void setup() {
  size(700, 700);
  
  baseKeyWidth = width/(2 * (float)numKeys);
  
  // Formule couleur
  pente = -0.66 / (redStart - blueEnd);
  ordOrig = -pente * redStart;
  println(pente + " -- " + ordOrig);
  
  for(int i = 0; i < numKeys; i++)
    velocities.add(0);
  
  g = createGraphics(width, height);
  g.beginDraw();
  g.noStroke();
  g.colorMode(HSB, 1.0);
  g.imageMode(CENTER);
  g.endDraw();
  tintInterval = 32;
  tintCounter = 0;
  
  MidiBus.list();
  myBus = new MidiBus(this, 0, -1);
}

void draw() {
  g.beginDraw();
  
  // Gerer effacement
  if(tintCounter == 0)
  {
    g.noTint();
    tintCounter++;
  }
  else if(tintCounter == tintInterval - 1)
  {
    g.tint(0.99);
    tintCounter = 0;
  }
  else
  {
    tintCounter++;
  }
  
  // Dessiner notes couleur dans buffer
  for(int i = 0; i < numKeys; i++)
  {
    Integer velocity = (Integer)velocities.get(i);  
    if(velocity > 0)
      {
          float intensity = (velocity / 127.0);
          float barHeight = 10; //intensity * height;
          float barTop = height/2 - barHeight/2;
          float barCenter = (0.5 + i) * baseKeyWidth;
          float barWidth = baseKeyWidth * (0.5 + 5*intensity);
          float barLeft = barCenter - barWidth / 2;
          
          float hue = pente * velocity + ordOrig;
          hue = min(max(hue, 0), 0.66); // entre rouge et bleu
          g.fill(hue, 1, 1);
          //g.fill(0, 1, intensity);
          g.rect(barLeft, barTop, barWidth, barHeight);
      }
  }
  g.endDraw();
  
  // Dessiner buffer dans fenetre
  background(0);
  image(g, 0, 0);
  
  g.beginDraw();
  
  // Prendre copie (essayer sans copie?)
  PImage copie = g.get();
  g.clear();
  
  // Transformer
  g.translate(width/2, height/2);
  g.scale(0.995); //0.97 0
  g.rotate(0.1); //0.01 0.01
  
  // Dessiner dans le buffer
  g.image(copie, 0, 0);
    
  g.endDraw();
}

void noteOn(int channel, int pitch, int velocity) {
  // Receive a noteOn 48-96
  // Velocity: 1-127
  Integer note = pitch - firstKey;
  if(note >= 0 && note < numKeys)
    velocities.set(note, velocity);
  
  /*
  println();
  println("Note On:");
  println("--------");
  println("Channel:"+channel);
  println("Pitch:"+pitch);
  println("Velocity:"+velocity);
  */
}

void noteOff(int channel, int pitch, int velocity) {
  // Receive a noteOff
  Integer note = pitch - firstKey;
  if(note >= 0 && note < numKeys)
    velocities.set(note, 0);
  
  /*
  println();
  println("Note Off:");
  println("--------");
  println("Channel:"+channel);
  println("Pitch:"+pitch);
  println("Velocity:"+velocity);
  */
}

void controllerChange(int channel, int number, int value) {
  // Receive a controllerChange
  println();
  println("Controller Change:");
  println("--------");
  println("Channel:"+channel);
  println("Number:"+number);
  println("Value:"+value);
}

void delay(int time) {
  int current = millis();
  while (millis () < current+time) Thread.yield();
}
